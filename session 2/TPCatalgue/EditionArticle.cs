﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPCatalgue
{
    public partial class EditionArticle : Form
    {
        CatalogueEntities ctx = new CatalogueEntities();    

        private Article article;

        public EditionArticle()
        {
            InitializeComponent();

            cbxRayon.ValueMember = "Id";
            cbxRayon.DisplayMember = "Libelle";
            cbxRayon.DataSource = ctx.Rayons.ToList();
            
            // Mode création :
            this.article = new Article();
            this.btnSupprimer.Visible = false;  
        }

        public EditionArticle(int idArticle) : this()
        {
            article = ctx.Articles.First(a => a.Id == idArticle);

            txtLibelle.Text = article.Libelle;
            txtDescription.Text = article.Description;
            txtPrix.Text = article.Prix.ToString();
            cbxRayon.SelectedValue = article.IdRayon;

            this.btnSupprimer.Visible = true;
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            article.Libelle = txtLibelle.Text;
            article.Description = txtDescription.Text;
            article.Prix = float.Parse(txtPrix.Text);
            article.IdRayon = (int)cbxRayon.SelectedValue;

            if(article.Id == 0)
            {
                // insert : 
                ctx.Articles.Add(article);
            }

            // si mode update, le contexte sait qu'il doit mettre à jour l'article :
            ctx.SaveChanges();

            this.Close();
            MessageBox.Show("Article enregistré avec succès");
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Supprimer cet article ?", "Suppression", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                ctx.Articles.Remove(article);
                ctx.SaveChanges();
                this.Close();
            }
        }
    }
}
