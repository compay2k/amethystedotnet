﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPCatalgue
{
    public partial class Catalogue : Form
    {
        CatalogueEntities ctx = new CatalogueEntities();    
        public Catalogue()
        {
            InitializeComponent();

            // Chargement de la combo Box :
            cbxRayon.ValueMember = "Id"; // valeur à récupérer
            cbxRayon.DisplayMember = "Libelle"; // valeur à afficher
            cbxRayon.DataSource = ctx.Rayons.ToList()
                                            .Prepend(new Rayon() { 
                                                        Id=0, 
                                                        Libelle="Tous les rayons"
                                            }).ToList(); // premier item par défaut

            ChargerListeArticles(ctx.Articles);
        }

        private void btnFiltrer_Click(object sender, EventArgs e)
        {
            int idRayon = (int)cbxRayon.SelectedValue;

            IQueryable<Article> query = 
                         from a in ctx.Articles
                         where (idRayon == 0 || a.Rayon.Id == (int)cbxRayon.SelectedValue)
                                && 
                                a.Libelle.Contains(textBox1.Text)
                         select a;

            ChargerListeArticles(query);
        }

        private void ChargerListeArticles(IQueryable<Article> articles)
        {
            dgvCatalogue.DataSource = articles.Select(a => new { 
                                                            a.Id,               
                                                            a.Libelle, 
                                                            a.Prix, 
                                                            a.Description, 
                                                            // alias obligatoire si 2 properties de même nom :
                                                            NomRayon = a.Rayon.Libelle 
                                                       }).ToList();
        }

        private void btnNouveau_Click(object sender, EventArgs e)
        {
            EditionArticle edition = new EditionArticle();
            OuvrirEdition(edition);
        }

        private void OuvrirEdition(EditionArticle edition)
        {
            edition.FormClosed += Edition_FormClosed;
            edition.ShowDialog();
        }

        private void Edition_FormClosed(object sender, FormClosedEventArgs e)
        {
            ctx = new CatalogueEntities();
            ChargerListeArticles(ctx.Articles);
        }

        private void dgvCatalogue_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int idArticle = (int)dgvCatalogue.Rows[e.RowIndex].Cells["Id"].Value;

            EditionArticle edition = new EditionArticle(idArticle);
            OuvrirEdition(edition);
        }
    }
}
