﻿namespace TPCatalgue
{
    partial class Catalogue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCatalogue = new System.Windows.Forms.DataGridView();
            this.cbxRayon = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnFiltrer = new System.Windows.Forms.Button();
            this.btnNouveau = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCatalogue)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCatalogue
            // 
            this.dgvCatalogue.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCatalogue.Location = new System.Drawing.Point(51, 129);
            this.dgvCatalogue.Name = "dgvCatalogue";
            this.dgvCatalogue.RowHeadersWidth = 62;
            this.dgvCatalogue.RowTemplate.Height = 28;
            this.dgvCatalogue.Size = new System.Drawing.Size(1322, 446);
            this.dgvCatalogue.TabIndex = 0;
            this.dgvCatalogue.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCatalogue_CellDoubleClick);
            // 
            // cbxRayon
            // 
            this.cbxRayon.FormattingEnabled = true;
            this.cbxRayon.Location = new System.Drawing.Point(132, 44);
            this.cbxRayon.Name = "cbxRayon";
            this.cbxRayon.Size = new System.Drawing.Size(364, 28);
            this.cbxRayon.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Rayon :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(552, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Libellé :";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(637, 44);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(369, 26);
            this.textBox1.TabIndex = 4;
            // 
            // btnFiltrer
            // 
            this.btnFiltrer.Location = new System.Drawing.Point(1064, 42);
            this.btnFiltrer.Name = "btnFiltrer";
            this.btnFiltrer.Size = new System.Drawing.Size(187, 41);
            this.btnFiltrer.TabIndex = 5;
            this.btnFiltrer.Text = "Filtrer";
            this.btnFiltrer.UseVisualStyleBackColor = true;
            this.btnFiltrer.Click += new System.EventHandler(this.btnFiltrer_Click);
            // 
            // btnNouveau
            // 
            this.btnNouveau.Location = new System.Drawing.Point(56, 597);
            this.btnNouveau.Name = "btnNouveau";
            this.btnNouveau.Size = new System.Drawing.Size(124, 41);
            this.btnNouveau.TabIndex = 6;
            this.btnNouveau.Text = "Nouveau";
            this.btnNouveau.UseVisualStyleBackColor = true;
            this.btnNouveau.Click += new System.EventHandler(this.btnNouveau_Click);
            // 
            // Catalogue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1403, 742);
            this.Controls.Add(this.btnNouveau);
            this.Controls.Add(this.btnFiltrer);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxRayon);
            this.Controls.Add(this.dgvCatalogue);
            this.Name = "Catalogue";
            this.Text = "Catalogue";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCatalogue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCatalogue;
        private System.Windows.Forms.ComboBox cbxRayon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnFiltrer;
        private System.Windows.Forms.Button btnNouveau;
    }
}