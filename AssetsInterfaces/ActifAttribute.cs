﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amethyste.AssetsManagement
{
    public class ActifAttribute : Attribute
    {
        public string TypeActif { get; set; }

        public ActifAttribute(string typeActif)
        {
            TypeActif = typeActif;
        }
    }
}
