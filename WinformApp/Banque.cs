﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amtehyste.Banque.App.Winform
{
    public partial class Banque : Form
    {
        public Banque()
        {
            InitializeComponent();
        }

        private void ButtonCreerCompte_Click(object sender, EventArgs e)
        {
            CreateAccount uc = new CreateAccount();

            // je m'abonne à l'événement CompteCree du uc :
            uc.CompteCree += Uc_CompteCree;

            OuvrirFenetre(uc);
        }

        private void Uc_CompteCree(object sender, CompteEventArgs e)
        {
            string message = string.Format("Compte numéro {0} créé avec succès : ", e.Compte.Numero);
            MessageBox.Show(message);

            ListeComptes uc = new ListeComptes();
            OuvrirFenetre(uc);
        }

        private void ButtonCreerPersonne_Click(object sender, EventArgs e)
        {
            CreerPersonnePhysique uc = new CreerPersonnePhysique();
            OuvrirFenetre(uc);
        }

        private void ButtonComptes_Click(object sender, EventArgs e)
        {
            ListeComptes uc = new ListeComptes();
            OuvrirFenetre(uc);
        }

        private void OuvrirFenetre(UserControl uc)
        {
            this.panelContenu.Controls.Clear();
            this.panelContenu.Controls.Add(uc);
            uc.Dock = DockStyle.Fill;
        }


    }
}
