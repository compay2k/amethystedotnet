﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amtehyste.Banque.App.Winform
{
    public partial class Exo1 : Form
    {
        private int nbLabels;

        public Exo1()
        {
            InitializeComponent();
        }

        private void AjouterItem()
        {
            Label lbl = new Label();
            lbl.Text = textBoxLibelle.Text;
            //lbl.Location = new Point(10, 30 * nbLabels++);
            this.panel1.Controls.Add(lbl);
            textBoxLibelle.Clear();
        }

        private void ButtonAjouter_Click(object sender, EventArgs e)
        {
            AjouterItem();
        }

        private void TextBoxLibelle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                AjouterItem();
            }
        }

        private void Exo1_Load(object sender, EventArgs e)
        {

        }
    }
}
