﻿using Amethyste.Banque.Business.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amtehyste.Banque.App.Winform
{
    class MockPersonnes
    {
        public static List<Personne> clients = new List<Personne>();

        static MockPersonnes()
        {
            clients.Add(new PersonnePhysique("11111", "Morisson", "Jim"));
            clients.Add(new PersonnePhysique("22222", "Winehouse", "Amy"));
            clients.Add(new PersonnePhysique("33333", "Gainsbourg", "Serge"));
        }
            

    }
}
