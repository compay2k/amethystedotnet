﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amethyste.Banque.Business.Personnes;

namespace Amtehyste.Banque.App.Winform
{
    public partial class CreerPersonnePhysique : UserControl
    {
        public CreerPersonnePhysique()
        {
            InitializeComponent();
        }

        private void ButtonEnregistrer_Click(object sender, EventArgs e)
        {
            string nom = textBoxNom.Text;
            string prenom = textBoxPrenom.Text;
            string identifiant = textBoxIdentifiant.Text;

            PersonnePhysique personne = new PersonnePhysique(identifiant, nom, prenom);

            MockPersonnes.clients.Add(personne);
        }
    }
}
