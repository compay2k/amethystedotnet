﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformApp
{
    public partial class Form1 : Form
    {
        int nbPlips = 0;

        public Form1()
        {
            InitializeComponent();
            this.labelMesssage.Text = "";
        }

        private void ButtonOK_Click(object sender, EventArgs e)
        {
            string nom = textBoxNom.Text;
            labelMesssage.Text = "Hello " + nom;
            labelExemple.Text = "plop";

            // afficher un textbox contenant "Plip"
            TextBox txt = new TextBox();
            txt.Text = "plip";
            txt.Location = new Point(0, 20 * nbPlips);
            nbPlips++;
            this.Controls.Add(txt);

        }

        private void Plop(object sender, EventArgs e)
        {
            MessageBox.Show("Plop");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void TextBoxNom_TextChanged(object sender, EventArgs e)
        {
            string nom = textBoxNom.Text;
            labelMesssage.Text = "Hello " + nom;
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void TextBoxNom_Click(object sender, EventArgs e)
        {
            
            if (textBoxNom.BackColor == Color.Red)
            {
                textBoxNom.BackColor = Color.Green;
            }
            else
            {
                textBoxNom.BackColor = Color.Red;
            }
        }

        private void TextBoxNom_Resize(object sender, EventArgs e)
        {

        }

        private void TextBoxNom_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MessageBox.Show("Gauche");
            }
            else
            {
                MessageBox.Show("Droit");
            }
        }
    }
}
