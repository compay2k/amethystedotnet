﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amethyste.Banque.Business.Comptes;
using Amethyste.Banque.Business.Personnes;
using System.IO;

namespace Amtehyste.Banque.App.Winform
{
    public partial class CreateAccount : UserControl
    {
        // 1 - définir un événement personnalisé :
        public event EventHandler<CompteEventArgs> CompteCree;

        public CreateAccount()
        {
            InitializeComponent();
        }

        private void CreateAccount_Load(object sender, EventArgs e)
        {
            // Remplir la combobox :
            comboBoxTitulaire.DataSource = MockPersonnes.clients;
            comboBoxTitulaire.DisplayMember = "Denomination";
        }

        private void ButtonEnregistrer_Click(object sender, EventArgs e)
        {

            try
            {
                // récupérer les valeurs saisies
                string numero = textBoxNumero.Text;
                double solde = double.Parse(textBoxSolde.Text);
                Personne titulaire = (Personne)comboBoxTitulaire.SelectedItem;

                // instancier un compte bancaire
                CompteBancaire compte = new CompteBancaire(numero);
                compte.Titulaire = titulaire;
                compte.Crediter(solde);

                MockComptes.comptes.Add(compte);

                // 2 - Déclenchement de l'événement CompteCree :
                if (CompteCree != null) // si j'ai des abonnés
                {
                    CompteCree(this, new CompteEventArgs(compte));
                }
            }
            catch(FormatException exc)
            {
                textBoxSolde.Clear();
                MessageBox.Show("Erreur de format, veuillez réessayer...");
            }
            catch (CompteBancaireException exc)
            {
                if (exc.Champ != "N/A")
                {
                    MessageBox.Show("Erreur sur le champ " + exc.Champ + " : " + exc.Message);
                }
                else
                {
                    MessageBox.Show("Erreur sur l'opération " + exc.Operation + " : " + exc.Message);
                }
                
            }

            
        }


    }
}
