﻿namespace Amtehyste.Banque.App.Winform
{
    partial class Banque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMenu = new System.Windows.Forms.Panel();
            this.buttonCreerPersonne = new System.Windows.Forms.Button();
            this.buttonCreerCompte = new System.Windows.Forms.Button();
            this.panelContenu = new System.Windows.Forms.Panel();
            this.buttonComptes = new System.Windows.Forms.Button();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.MediumPurple;
            this.panelMenu.Controls.Add(this.buttonComptes);
            this.panelMenu.Controls.Add(this.buttonCreerPersonne);
            this.panelMenu.Controls.Add(this.buttonCreerCompte);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(321, 792);
            this.panelMenu.TabIndex = 0;
            // 
            // buttonCreerPersonne
            // 
            this.buttonCreerPersonne.Location = new System.Drawing.Point(53, 181);
            this.buttonCreerPersonne.Name = "buttonCreerPersonne";
            this.buttonCreerPersonne.Size = new System.Drawing.Size(211, 63);
            this.buttonCreerPersonne.TabIndex = 1;
            this.buttonCreerPersonne.Text = "Nouvelle Personne";
            this.buttonCreerPersonne.UseVisualStyleBackColor = true;
            this.buttonCreerPersonne.Click += new System.EventHandler(this.ButtonCreerPersonne_Click);
            // 
            // buttonCreerCompte
            // 
            this.buttonCreerCompte.Location = new System.Drawing.Point(53, 76);
            this.buttonCreerCompte.Name = "buttonCreerCompte";
            this.buttonCreerCompte.Size = new System.Drawing.Size(211, 63);
            this.buttonCreerCompte.TabIndex = 0;
            this.buttonCreerCompte.Text = "Nouveau Compte";
            this.buttonCreerCompte.UseVisualStyleBackColor = true;
            this.buttonCreerCompte.Click += new System.EventHandler(this.ButtonCreerCompte_Click);
            // 
            // panelContenu
            // 
            this.panelContenu.BackColor = System.Drawing.Color.LightBlue;
            this.panelContenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContenu.Location = new System.Drawing.Point(321, 0);
            this.panelContenu.Name = "panelContenu";
            this.panelContenu.Size = new System.Drawing.Size(1050, 792);
            this.panelContenu.TabIndex = 1;
            // 
            // buttonComptes
            // 
            this.buttonComptes.Location = new System.Drawing.Point(55, 281);
            this.buttonComptes.Name = "buttonComptes";
            this.buttonComptes.Size = new System.Drawing.Size(211, 63);
            this.buttonComptes.TabIndex = 2;
            this.buttonComptes.Text = "Liste des comptes";
            this.buttonComptes.UseVisualStyleBackColor = true;
            this.buttonComptes.Click += new System.EventHandler(this.ButtonComptes_Click);
            // 
            // Banque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1371, 792);
            this.Controls.Add(this.panelContenu);
            this.Controls.Add(this.panelMenu);
            this.Name = "Banque";
            this.Text = "Banque";
            this.panelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Button buttonCreerCompte;
        private System.Windows.Forms.Panel panelContenu;
        private System.Windows.Forms.Button buttonCreerPersonne;
        private System.Windows.Forms.Button buttonComptes;
    }
}