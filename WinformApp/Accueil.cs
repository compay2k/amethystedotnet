﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amtehyste.Banque.App.Winform
{
    public partial class Accueil : Form
    {
        public Accueil()
        {
            InitializeComponent();
        }

        private void FichierToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void SDIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Exo1 f = new Exo1();
            // ouverture non modale :
            // f.Show();
            // ouverture modale :
            f.ShowDialog();
        }

        private void MDIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // IsMDIContainer : true

            // je referme mes enfants MDI
            foreach (Form child in this.MdiChildren)
            {
                child.Close();
            }
            // j'ouvre un nouveau formulaire en mdi :
            Exo1 f = new Exo1();
            f.MdiParent = this;

            f.FormBorderStyle = FormBorderStyle.None; // pas de bordures
            f.Dock = DockStyle.Fill; // j'étends à tout l'espace disponible

            f.Show();
        }

        private void BanqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Banque b = new Banque();
            b.ShowDialog();
        }
    }
}
