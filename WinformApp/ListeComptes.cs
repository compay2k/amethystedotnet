﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Amethyste.Banque.Business.Comptes;

namespace Amtehyste.Banque.App.Winform
{
    public partial class ListeComptes : UserControl
    {
        public ListeComptes()
        {
            InitializeComponent();
        }

        private void ListeComptes_Load(object sender, EventArgs e)
        {
            listViewComptes.DataSource = MockComptes.comptes;
        }
    }
}
