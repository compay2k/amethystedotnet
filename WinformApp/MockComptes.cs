﻿using Amethyste.Banque.Business.Comptes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amtehyste.Banque.App.Winform
{
    class MockComptes
    {
        public static List<CompteBancaire> comptes = new List<CompteBancaire>();

        static MockComptes()
        {
            comptes.Add(new CompteBancaire("AAAAA"));
            comptes.Add(new CompteBancaire("BBBBB"));
            comptes.Add(new CompteBancaire("CCCCC"));
        }
    }
}
