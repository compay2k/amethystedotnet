﻿namespace Amtehyste.Banque.App.Winform
{
    partial class CreateAccount
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNumero = new System.Windows.Forms.Label();
            this.textBoxNumero = new System.Windows.Forms.TextBox();
            this.labelSolde = new System.Windows.Forms.Label();
            this.textBoxSolde = new System.Windows.Forms.TextBox();
            this.buttonEnregistrer = new System.Windows.Forms.Button();
            this.comboBoxTitulaire = new System.Windows.Forms.ComboBox();
            this.labelTitulaire = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelNumero
            // 
            this.labelNumero.AutoSize = true;
            this.labelNumero.Location = new System.Drawing.Point(72, 58);
            this.labelNumero.Name = "labelNumero";
            this.labelNumero.Size = new System.Drawing.Size(65, 20);
            this.labelNumero.TabIndex = 0;
            this.labelNumero.Text = "Numero";
            // 
            // textBoxNumero
            // 
            this.textBoxNumero.Location = new System.Drawing.Point(179, 52);
            this.textBoxNumero.Name = "textBoxNumero";
            this.textBoxNumero.Size = new System.Drawing.Size(439, 26);
            this.textBoxNumero.TabIndex = 1;
            // 
            // labelSolde
            // 
            this.labelSolde.AutoSize = true;
            this.labelSolde.Location = new System.Drawing.Point(72, 111);
            this.labelSolde.Name = "labelSolde";
            this.labelSolde.Size = new System.Drawing.Size(50, 20);
            this.labelSolde.TabIndex = 2;
            this.labelSolde.Text = "Solde";
            // 
            // textBoxSolde
            // 
            this.textBoxSolde.Location = new System.Drawing.Point(179, 108);
            this.textBoxSolde.Name = "textBoxSolde";
            this.textBoxSolde.Size = new System.Drawing.Size(439, 26);
            this.textBoxSolde.TabIndex = 3;
            // 
            // buttonEnregistrer
            // 
            this.buttonEnregistrer.Location = new System.Drawing.Point(76, 256);
            this.buttonEnregistrer.Name = "buttonEnregistrer";
            this.buttonEnregistrer.Size = new System.Drawing.Size(189, 48);
            this.buttonEnregistrer.TabIndex = 4;
            this.buttonEnregistrer.Text = "Enregistrer";
            this.buttonEnregistrer.UseVisualStyleBackColor = true;
            this.buttonEnregistrer.Click += new System.EventHandler(this.ButtonEnregistrer_Click);
            // 
            // comboBoxTitulaire
            // 
            this.comboBoxTitulaire.FormattingEnabled = true;
            this.comboBoxTitulaire.Location = new System.Drawing.Point(179, 180);
            this.comboBoxTitulaire.Name = "comboBoxTitulaire";
            this.comboBoxTitulaire.Size = new System.Drawing.Size(377, 28);
            this.comboBoxTitulaire.TabIndex = 5;
            // 
            // labelTitulaire
            // 
            this.labelTitulaire.AutoSize = true;
            this.labelTitulaire.Location = new System.Drawing.Point(75, 184);
            this.labelTitulaire.Name = "labelTitulaire";
            this.labelTitulaire.Size = new System.Drawing.Size(64, 20);
            this.labelTitulaire.TabIndex = 6;
            this.labelTitulaire.Text = "Titulaire";
            // 
            // CreateAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelTitulaire);
            this.Controls.Add(this.comboBoxTitulaire);
            this.Controls.Add(this.buttonEnregistrer);
            this.Controls.Add(this.textBoxSolde);
            this.Controls.Add(this.labelSolde);
            this.Controls.Add(this.textBoxNumero);
            this.Controls.Add(this.labelNumero);
            this.Name = "CreateAccount";
            this.Size = new System.Drawing.Size(695, 395);
            this.Load += new System.EventHandler(this.CreateAccount_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNumero;
        private System.Windows.Forms.TextBox textBoxNumero;
        private System.Windows.Forms.Label labelSolde;
        private System.Windows.Forms.TextBox textBoxSolde;
        private System.Windows.Forms.Button buttonEnregistrer;
        private System.Windows.Forms.ComboBox comboBoxTitulaire;
        private System.Windows.Forms.Label labelTitulaire;
    }
}
