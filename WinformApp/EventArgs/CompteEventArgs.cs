﻿using Amethyste.Banque.Business.Comptes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amtehyste.Banque.App.Winform
{
    public class CompteEventArgs : EventArgs
    {
        public CompteBancaire Compte { get; set; }

        public CompteEventArgs(CompteBancaire compte)
        {
            Compte = compte;
        }
    }
}
