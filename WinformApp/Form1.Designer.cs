﻿namespace WinformApp
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNom = new System.Windows.Forms.Label();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.labelMesssage = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.labelExemple = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.Location = new System.Drawing.Point(44, 43);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(95, 20);
            this.labelNom.TabIndex = 0;
            this.labelNom.Text = "Votre nom : ";
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(169, 40);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(351, 26);
            this.textBoxNom.TabIndex = 1;
            this.textBoxNom.Click += new System.EventHandler(this.TextBoxNom_Click);
            this.textBoxNom.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TextBoxNom_MouseClick);
            this.textBoxNom.TextChanged += new System.EventHandler(this.TextBoxNom_TextChanged);
            this.textBoxNom.Resize += new System.EventHandler(this.TextBoxNom_Resize);
            // 
            // labelMesssage
            // 
            this.labelMesssage.AutoSize = true;
            this.labelMesssage.Location = new System.Drawing.Point(53, 193);
            this.labelMesssage.Name = "labelMesssage";
            this.labelMesssage.Size = new System.Drawing.Size(33, 20);
            this.labelMesssage.TabIndex = 2;
            this.labelMesssage.Text = "......";
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(48, 95);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(166, 49);
            this.buttonOK.TabIndex = 3;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.ButtonOK_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(371, 108);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(113, 24);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // labelExemple
            // 
            this.labelExemple.Location = new System.Drawing.Point(310, 166);
            this.labelExemple.Name = "labelExemple";
            this.labelExemple.Size = new System.Drawing.Size(100, 23);
            this.labelExemple.TabIndex = 5;
            this.labelExemple.Text = "EXEMPLE";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(532, 212);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 35);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Plop);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 271);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelMesssage);
            this.Controls.Add(this.textBoxNom);
            this.Controls.Add(this.labelNom);
            this.Controls.Add(this.labelExemple);
            this.Name = "Form1";
            this.Text = "Mon Formulaire";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.Label labelMesssage;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.CheckBox checkBox1;

        // 1 - Je déclare mon contrôle :
        private System.Windows.Forms.Label labelExemple;
        private System.Windows.Forms.Button button1;
    }
}

