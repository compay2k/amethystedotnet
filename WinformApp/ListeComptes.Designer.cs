﻿namespace Amtehyste.Banque.App.Winform
{
    partial class ListeComptes
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewComptes = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // listViewComptes
            // 
            this.listViewComptes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listViewComptes.Location = new System.Drawing.Point(28, 30);
            this.listViewComptes.Name = "listViewComptes";
            this.listViewComptes.Size = new System.Drawing.Size(1132, 449);
            this.listViewComptes.TabIndex = 0;
            // 
            // ListeComptes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGreen;
            this.Controls.Add(this.listViewComptes);
            this.Name = "ListeComptes";
            this.Size = new System.Drawing.Size(1189, 509);
            this.Load += new System.EventHandler(this.ListeComptes_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listViewComptes;
    }
}
