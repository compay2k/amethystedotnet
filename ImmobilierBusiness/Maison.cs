﻿using Amethyste.AssetsManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amethyste.Immobilier.Business
{
    public class Maison : IActif, ICloneable
    {
        public int Surface { get; set; }
        public int PrixM2 { get; set; }

        public Maison(int surface, int prixM2)
        {
            //Thread.Sleep(3000);
            Surface = surface;
            PrixM2 = prixM2;
        }

        public double GetValorisation()
        {
            return Surface * PrixM2;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
