﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Amethyste.AssetsManagement;
using Amethyste.Banque.Business.Comptes;
using Amethyste.Banque.Business.Personnes;
using Amethyste.Banque.Business.Prets;
using Amethyste.Immobilier.Business;

namespace Amethyste.Banque.ConsoleApp
{
    // Structuration
    // static
    // Interfaces
    // types nullables
    // IEnumerable / IEnumerator
    // Operateurs ternaires
    // Collections
    // Generics
    // Reflection
    // introduction aux Windows Forms
    // delegués/événements
    // architecture winforms
    // composants graphiques de bases
    // cinématique écrans
    // user controls
    // exceptions personnalisées
    // TODO composants devexpress
    // TODO enums & constantes regroupées
    // TODO lambdas

    class Program
    {
        static void Main(string[] args)
        {
            DemoReflection();

            Console.ReadLine();
        }
                
        static void DemoReflection()
        {
            // string path = "c:\\qsdqs\\qsdqsd";
            string path = @"c:\qsdqs\qsdqsd";

            string className = "Amethyste.Banque.Business.Comptes.CompteBancaire, Amethyste_BanqueBusiness, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null";
            Type t = Type.GetType(className); 

            //Type t = typeof(CompteBancaire);

            if (t.IsClass && !t.IsAbstract)
            {
                ConstructorInfo[] ctors = t.GetConstructors();
                foreach (ConstructorInfo ctor in ctors)
                {
                    Console.WriteLine(ctor);

                    // j'instancie un objet de type t : 
                    object[] parametresConstr = {"424242"};
                    object o = ctor.Invoke(parametresConstr);

                    // j'appelle la méthode Crediter :
                    MethodInfo mi = t.GetMethod("Crediter");
                    object[] parametresMeth = { 500 };
                    mi.Invoke(o, parametresMeth);

                    //FieldInfo fi = t.GetField("solde");
                    //fi.SetValue(o, 3600000000);

                    Console.WriteLine(o);


                }
            }
        }

        static void Divers()
        {
            // nullable
            int? x = null;
            DateTime? plop = null;

            if (x.HasValue)
            {
                int y = x.Value + 1;
            }

            #region statiques
            PersonnePhysique p = new PersonnePhysique("12345", "Spears", "Britney");
            p.DateNaissance = new DateTime(2002, 03, 21);

            Console.WriteLine(p.EstMajeure);

            PersonnePhysique.AgeMajorite = 21;

            Console.WriteLine(p.EstMajeure);
            #endregion

            #region banque
            CompteEpargne cpt1 = new CompteEpargne("132145646", 0.1f, 10000000);
            cpt1.Titulaire = new PersonneMorale("12345", "Améthyste");
            cpt1.Crediter(500);

            //Emprunt<CompteBancaire> emp = new Emprunt<CompteBancaire>();
            //emp.Caution = new CompteBancaire("1111");
            Emprunt<CompteEpargne> emp = new Emprunt<CompteEpargne>();
            emp.Caution = cpt1;

            emp.CalculerMontant();

            //System.Speech.Synthesis.SpeechSynthesizer s = new System.Speech.Synthesis.SpeechSynthesizer();
            //s.Speak(cpt1.Titulaire.GetDenomination());
            #endregion

            #region immo
            Console.Write("Creation maison...");
            Maison m = new Maison(120, 4000);
            Console.WriteLine("...Done !");

            Maison m2 = (Maison)m.Clone();
            Console.WriteLine(m2.Surface);
            #endregion

            #region actifs

            Portefeuille pf = new Portefeuille();

            pf.AjouterActif(cpt1);
            // Opérateur ternaire :
            string mot = (pf.NbActifs < 2) ? "actif" : "actifs";
            Console.WriteLine("Il y a {0} {1} dans le portefeuille", pf.NbActifs, mot);
            pf.AjouterActif(m);

            //Maison m3 = pf.Echanger<Maison>(m2, 0);

            foreach (IActif a in pf)
            {
                Console.WriteLine(a);
            }
            
            Console.WriteLine(pf.ValorisationTotale);

            #endregion
        }
    }
}
