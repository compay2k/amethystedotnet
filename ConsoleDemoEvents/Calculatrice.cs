﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDemoEvents
{
    class Calculatrice
    {
        public delegate double Calcul(double a, double b);

        public void EffectuerOperation(Calcul calculer)
        {
            Console.Write("Valeur de a : ");
            double a = double.Parse(Console.ReadLine());

            Console.Write("Valeur de b : ");
            double b = double.Parse(Console.ReadLine());

            double resultat = calculer(a, b);

            Console.WriteLine("Résultat : {0}", resultat);
        }
    }
}
