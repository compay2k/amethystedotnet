﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDemoEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculatrice calc = new Calculatrice();

            calc.EffectuerOperation(additionner);

            Console.ReadLine();
        }

        public static double additionner(double a, double b)
        {
            return a + b;
        }
    }
}
