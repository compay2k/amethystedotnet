﻿using Amethyste.Banque.Business.Comptes;
using Amethyste.Banque.Business.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amethyste.Banque.Business.Prets
{
    public class Emprunt<T>
                   where T : CompteEpargne
    {
        public T Caution { get; set; }

        public int CalculerMontant()
        {
            int result = 0;
            result += (int)Caution.Solde;       
            return result;
        } 
    }
}
