﻿using System;


namespace Amethyste.Banque.Business.Personnes
{
    public class PersonneMorale : Personne
    {
        #region PROPERTIES
        public string RaisonSociale { get; set; }
        public string SIRET { get; set; }
        #endregion

        #region CONSTRUCTORS
        public PersonneMorale(string identifiant, string raisonSociale) : base(identifiant)
        {
            RaisonSociale = raisonSociale;
        }
        #endregion

        #region METHODS
        public override string Denomination
        {
            get
            {
                return RaisonSociale;
            }
        }

        #endregion

    }
}
