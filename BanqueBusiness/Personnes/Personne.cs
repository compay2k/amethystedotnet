﻿using System;


namespace Amethyste.Banque.Business.Personnes
{
    public abstract class Personne
    {
        #region PROPERTIES
        public string Identifiant { get; set; }
        #endregion

        #region CONSTRUCTORS
        protected Personne(string identifiant)
        {
            Identifiant = identifiant;
        }

        public abstract string Denomination { get; }

        #endregion
    }
}
