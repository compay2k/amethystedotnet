﻿using System;

namespace Amethyste.Banque.Business.Personnes
{
    public class PersonnePhysique : Personne
    {
        public static int AgeMajorite = 18;
        private static int nbInstances = 0;
        
        #region PROPERTIES
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime DateNaissance { get; set; }

        public bool EstMajeure {
            get
            {
                return DateNaissance.AddYears(AgeMajorite) <= DateTime.Now;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public PersonnePhysique(string identifiant, string nom, string prenom) : base(identifiant)
        {
            nbInstances++;
            Nom = nom;
            Prenom = prenom;
        }

        #endregion

        #region METHODS
        public override string Denomination
        {
            get
            {
                return Prenom + " " + Nom;
            }
        }

        #endregion

    }
}
