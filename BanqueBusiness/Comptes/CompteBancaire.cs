﻿using System;
using Amethyste.Banque.Business.Personnes;
using Amethyste.AssetsManagement;
using System.Text.RegularExpressions;

namespace Amethyste.Banque.Business.Comptes
{
    [Actif("primaire")]
    public class CompteBancaire
    {
        protected const double COMMISSION_CREDIT = 0.99;

        #region PROPERTIES

        public double Solde { get; protected set; }
        public string Numero { get; }
        public DateTime DateCreation { get; }
        public Personne Titulaire { get; set; }

        #endregion

        #region CONSTRUCTORS

        public CompteBancaire(String numero)
        {
            //Regex re = new Regex("[A-Z]{3}[1-9]{6}");
            //if (!re.Match(numero).Success)
            if (numero.Length < 3)
            {
                throw new CompteBancaireException("Le numero doit faire au moins 3 caracteres", "NUMERO");
            }
            this.Numero = numero;
            this.DateCreation = DateTime.Now;
        }

        #endregion

        #region METHODS

        public virtual void Crediter(double montant)
        {
            if (montant > 9999)
            {
                throw new CompteBancaireException("Crédit impossible, montant trop élevé",operation:"CREDIT");
            }

            Solde += montant * COMMISSION_CREDIT;
        }

        public virtual void Debiter(double montant)
        {
            Solde -= montant;
        }

        public override string ToString()
        {
            return string.Format("Compte numéro {0} - Solde : {1:c} - Créé le {2} - Détenu par {3}", this.Numero, this.Solde, this.DateCreation, this.Titulaire?.Denomination);
        }

        #endregion


    }

    
}
