﻿using Amethyste.AssetsManagement;
using System;

namespace Amethyste.Banque.Business.Comptes
{
    public class CompteEpargne : CompteBancaire, IActif
    {
        private const int DELAI_REGLEMENTAIRE_DEBIT = 3;
        private const double COMMISSION_CREDIT_EPARGNE = 0.95;

        #region PROPERTIES
        public float TauxInteret { get; set; }
        public double Plafond { get; set; }
        #endregion

        #region CONSTRUCTORS

        public CompteEpargne(string numero, float tauxInteret, double plafond) : base(numero)
        {
            this.TauxInteret = tauxInteret;
            this.Plafond = plafond;
        }

        #endregion

        #region METHODS


        public override void Debiter(double montant)
        {
            if (this.DateCreation.AddMonths(DELAI_REGLEMENTAIRE_DEBIT) < DateTime.Now)
            {
                base.Debiter(montant);
            }
            else
            {
                throw new Exception(String.Format("opération annulée : retrait impossible avant {0} mois", DELAI_REGLEMENTAIRE_DEBIT));
            }
        }

        public override void Crediter(double montant)
        {
            if (Solde + montant < Plafond)
            {
                Solde += montant * COMMISSION_CREDIT_EPARGNE;
            }
            else
            {
                // je lève une exception :
                throw new Exception("opération annulée : dépassement du plafond");
            }
        }

        public double DefinirInterets()
        {
            return this.TauxInteret * this.Solde;
        }

        public double GetValorisation()
        {
            return Solde + DefinirInterets();
        }

        #endregion
    }
}
