﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amethyste.Banque.Business.Comptes
{
    public class CompteBancaireException : Exception
    {
        public string Champ { get; set; }
        public string Operation { get; set; }

        public CompteBancaireException(string message) : base(message)
        {
        }

        public CompteBancaireException(string message, string champ) : base(message)
        {
            this.Champ = champ;
        }

        public CompteBancaireException(string message, string operation, string champ = "N/A") : this(message, champ)
        {
            this.Operation = operation; 
        }

    }
}
