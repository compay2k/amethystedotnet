﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amethyste.AssetsManagement
{
    internal class PortefeuilleEnumerator : IEnumerator<IActif>
    {
        public Portefeuille Portefeuille { get; set; }

        public PortefeuilleEnumerator(Portefeuille portefeuille)
        {
            Portefeuille = portefeuille;
        }

        private int index = -1;

        public IActif Current
        {
            get
            {
                int nbActifsPrimaires = Portefeuille.ActifsPrincipaux.Count;
                if (index < nbActifsPrimaires)
                {
                    return Portefeuille.ActifsPrincipaux[index];
                }
                else
                {
                    return Portefeuille.ActifsSecondaires[index - nbActifsPrimaires];
                }
            }
        }

        object IEnumerator.Current
        {
            get
            {
                int nbActifsPrimaires = Portefeuille.ActifsPrincipaux.Count;
                if (index < nbActifsPrimaires)
                {
                    return Portefeuille.ActifsPrincipaux[index];
                }
                else
                {
                    return Portefeuille.ActifsSecondaires[index - nbActifsPrimaires];
                }
            }
        }

        public bool MoveNext()
        {
            index++;
            if (index < Portefeuille.ActifsPrincipaux.Count + Portefeuille.ActifsSecondaires.Count)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            this.index = 0;
        }

        public void Dispose()
        {
            
        }
    }
}
