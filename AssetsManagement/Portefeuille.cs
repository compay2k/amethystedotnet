﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amethyste.AssetsManagement
{
    public class Portefeuille : IEnumerable<IActif>
    {
        #region PROPERTIES
        internal List<IActif> ActifsPrincipaux { get; set; }
        internal List<IActif> ActifsSecondaires { get; set; }
        public int NbActifs
        {
            get
            {
                return ActifsPrincipaux.Count + ActifsSecondaires.Count;
            }
        }

        public double ValorisationTotale
        {
            get
            {
                double total = 0;
                foreach (IActif actif in ActifsPrincipaux)
                {
                    total += actif.GetValorisation();
                }
                return total;
            }
        }
        #endregion

        #region CONSTRUCTORS
        public Portefeuille()
        {
            ActifsPrincipaux = new List<IActif>();
            ActifsSecondaires = new List<IActif>();
        }
        #endregion

        #region METHODS

        public T Echanger<T>(T a, int index) where T:IActif
        {
            T result = (T)ActifsPrincipaux[index];

            ActifsPrincipaux[index] = a;

            return result;
        }

        public void AjouterActif(IActif actif)
        {
            if (actif.GetValorisation() < 1000)
            {
                //ActifsSecondaires.Add(actif);
                ActifsPrincipaux.Add(actif);
            }
            else
            {
                ActifsPrincipaux.Add(actif);
            }
        }

        public IEnumerator<IActif> GetEnumerator()
        {
            return new PortefeuilleEnumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new PortefeuilleEnumerator(this);
        }
        #endregion
    }
}
